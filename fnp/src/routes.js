import Home from './components/Home.vue';
import Bolsa from './components/bolsa/Bolsa.vue';
import Mercadito from './components/mercadito/Mercadito.vue';

export const routes = [
  { path: '/', component: Home },
  { path: '/Bolsa', component: Bolsa },
  { path: '/Mercadito', component: Mercadito }
]
